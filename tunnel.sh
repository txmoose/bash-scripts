#!/bin/bash
# -----------------------------------------------------------------------------
# [Author]      Kyle Brieden
# [Title]       SSH tunnel wrapper
# [Description] Simple wrapper to set up SSH tunnels faster
# -----------------------------------------------------------------------------

VERSION=0.1
USAGE="Usage:\ttunnel <port-to-bind> <address-to-bind> <host>\n\ttunnel <local-port-to-bind> <remote-port-to-bind> <address-to-bind> <host>"

# --- Ensure enough parameters sent -------------------------------------------
if [ $# -lt 3 ] || [ $# -gt 4 ] ; then
    /bin/echo -e $USAGE
    exit 1;
fi

# --- Body --------------------------------------------------------------------
if [ $# -eq 3 ] ; then
    /usr/bin/ssh -qNfCL $1:$2:$1 $3
else
    /usr/bin/ssh -qNfCL $2:$3:$1 $4
fi
